# Genome Accessibility Mask

This repository contains the 1000 Genomes Accessibility Mask, lifted over to Human Genome build 38 coordinates.

* downloaded from ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/phase1/analysis_results/supporting/accessible_genome_masks/
* lifted over using UCSC liftOver (https://genome-store.ucsc.edu), with the hg19 to b38 chain file (http://hgdownload.cse.ucsc.edu/downloads.html)

## Files
* **`20120824_strict_mask.b38.bed`** : Lifted over mask (this should be the file you are interested in most of the time)
* **`20120824_strict_mask.b38.complement.bed`** : Complemented BED, all regions _not_ covered by the mask
* **`20120824_strict_mask.b38.unmapped.bed`** : regions that _failed_ to be lifted over

## Contact

Arthur Gilly (ag15 at sanger dot ac dot uk)